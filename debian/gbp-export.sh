#!/bin/sh

gbp export-orig --pristine-tar --component=opentest4j --component=apiguardian --component=hawt-jni --component=jansi1 --component=jline3 --component=junit4 --component=junit5 --component=libhamcrest-java --component=opentest4j-reporting --component=picocli --component=testng --component=univocity-parsers --compression=gzip
